import React from 'react'
import styled from 'styled-components'
import { Nav, Figure, Navbar } from 'react-bootstrap'
import { Link } from 'react-router-dom'


const Header = () => {
    
    const menu = [
        {
            title: "Home",
            link: '',
            icons: ''
        },
        {
            title: "About",
            link: 'about',
            icons: ''
        },
        {
            title: "Products",
            link: 'products',
            icons: ''
        },
        {
            title: "Showcase",
            link: 'showcase',
            icons: ''
        },
        {
            title: "Contact",
            link: 'contact',
            icons: ''
        },
    ]
    
    return (

            <Menu>
                <Figure>
                <img src="https://i.imgur.com/NPsGiJv.png" 
                style={{width: '100px'}}
                alt="logo"
                />
                </Figure>
                    {menu.map((item, i) => (
                        <Link to={item.link}>
                            <Nav.Link  key={i} href="home">{item.title}</Nav.Link>
                        </Link>
                        
                    ))}                      
            </Menu>
           
    )
}

const Menu = styled(Navbar)`
    background-color:black;
    width: 100%;

  a {
    font-size: 1rem;
    text-transform: uppercase;
    padding: 1rem 0.5rem;
    font-weight: bold;
    letter-spacing: 0.5rem;
    transition: color 0.3s linear;
  }

  img {
    padding: 1rem 0.7rem 0rem;
  }
  `

export default Header
