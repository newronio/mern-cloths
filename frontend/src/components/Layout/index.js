import React from 'react'
import styled from 'styled-components'
import Header from './header'
import Footer from './footer'

const Layout = ({ children }) => {
    return (
        <>

            <HeaderStyled>
                <Header/>
            </HeaderStyled>
                <Content> 
                    {children} 
                </Content>             
            <FooterStyled>
                <Footer/>
            </FooterStyled>
            
        </>
    )
}


const HeaderStyled = styled.div `
    
`

const Content = styled.div `
    min-height: 700px;

`

const FooterStyled = styled.div `

`

export default Layout
