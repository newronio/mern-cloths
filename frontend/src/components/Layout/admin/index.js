import React from 'react'
import '../../../assets/styles/css/aaa.css'

import Sidebar from './sidebar'
import HeaderTest from './header'
const LayoutAdmin = ({ children, Menu }) => {
    return (
        <div id="wrapper">
            <Sidebar Menu={Menu} />
            <div id="content-wrapper" className="d-flex flex-column">
                <div id="content">
                    <HeaderTest />
                    <div className="container-fluid">
                        {children}
                    </div>
                </div>

            </div>
        </div>

    )
}

export default LayoutAdmin
