import React from 'react'
import styled from 'styled-components'
export default ({title, sub}) => {
    return (
        <ContainerTitle>
            <Title>{title}</Title>
            <Sub>{sub}</Sub>
        </ContainerTitle>
    )
}

const Title = styled.div`
    color: white;
    font-size: 20px;
    font-weight: 600;
`
const ContainerTitle = styled.div`
    background: black;
    color: white;   
    padding:15px;
    padding-inline-start: 20px;
    border-bottom: 2px solid white;
    border-top: 2px solid white;
`

const Sub = styled.div`
`
