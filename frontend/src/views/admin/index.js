import React from 'react'
import { Route } from 'react-router-dom'
import LayoutAdmin from '../../components/Layout/admin/index'
import Dash from './dash'

import { FaTachometerAlt, FaImage, FaTools, FaTags, FaProductHunt } from 'react-icons/fa'


const MenuItens = [
    {
        name: "DashBoard",
        path: '/',
        icon: <FaTachometerAlt />,
        component: () => <Dash />
    },
    {
        name: "Banner",
        path: '/banner',
        icon: <FaImage />,
        component: () => <h1>Banner</h1>
    },
    {
        name: "Categories",
        path: '/categories',
        icon: <FaTags />,
        component: () => <h1>Categories</h1>
    },
    {
        name: "Products",
        path: '/products',
        icon: <FaProductHunt />,
        component: () => <h1>Products</h1>
    },
    {
        name: "Showcase",
        path: '/showcases',
        icon: <FaTools />,
        component: () => <h1>Showcase</h1>
    },
]


export default (props) => {

    return (
        <LayoutAdmin Menu={MenuItens}>
            {MenuItens.map((item, i) => (
                <Route key={i} exact basename={props.match.path} path={props.match.path + item.path} component={item.component} />
            ))}
        </LayoutAdmin>
    )
}
