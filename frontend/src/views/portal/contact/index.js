import React from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import styled from 'styled-components'
import TitlePage from '../../../components/titlePage'

//import { FcBearish } from "react-icons/fc";

export default () => {
    return (
        <Contact>
            <TitlePage title="Contacts" sub="Get in touch with us!"/>
                <Container>
                        <Row>
                            <Info md={4}>INFORMAÇÕES</Info>
                            <Form md={8}>FORMULÁRIO</Form>
                        </Row>              
            </Container>            
        </Contact>
    )
}

const Contact = styled.div `
    display: block;
    height: 500px;
`

const Info = styled(Col)`
  background: red;
  width: 100%;
  height: 300px;
`
const Form = styled(Col)`
  background: blue;
  width: 100%;
  height: 300px;
`