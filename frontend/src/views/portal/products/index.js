import React from 'react'
import { Container, Tab, Tabs } from 'react-bootstrap'
import styled from 'styled-components'
import TitlePage from '../../../components/titlePage'
//import { FcBearish } from "react-icons/fc";

export default () => {
    return (

        <Products>
            <TitlePage title="Products" sub="Some of our products!"></TitlePage>
            <Container>
                <TabBox defaultActiveKey="profile" id="uncontrolled-tab-example">
                    <TabItem eventKey="social_shirt" title="Social Shirt">
                        sasda
                    </TabItem>
                    <TabItem eventKey="tuxedo" title="Tuxedo">
                        sdaasd
                    </TabItem>
                    <TabItem eventKey="Pants" title="Pants">
                        asdasd
                    </TabItem>
                </TabBox>
            </Container>
        </Products>
    )
}

const Products = styled.div `
    display: block;
    height: 500px;

    .tab-content{
        background: white;
    }
`
const TabBox = styled(Tabs)`
    background: white;
`

const TabItem = styled(Tab)`
    background: white;
`