import React from 'react'
import styled from 'styled-components'
import Banner from './banner'
import About from './about'
import Showcase from './showcase'
import Products from './products'
import Info from './info'

export default () => {
    return (
        <ContainerHome>
            <Banner/>
            <Info/>
            <About/>
            <Showcase/>
            <Products/>
        </ContainerHome>
    )
}



const ContainerHome = styled.div`
    
`