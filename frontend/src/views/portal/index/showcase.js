import React from 'react'
import styled from 'styled-components'

const ShowcaseHome = () => {
    return (
        <Showcase>
        </Showcase>
    )
}

export default ShowcaseHome

const Showcase = styled.div`
    height: 400px;
    width: 100%;
    background: #70a1ff
`