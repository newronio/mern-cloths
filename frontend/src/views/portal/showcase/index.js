import React from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import styled from 'styled-components'
import TitlePage from '../../../components/titlePage'
//import { FcBearish } from "react-icons/fc";

export default () => {
    return (

        <Showcase>
            <TitlePage title="Showcase" sub="All of our products!"></TitlePage>
            <Container>
                    <Row>
                        <ShowcaseItem>1</ShowcaseItem>
                        <ShowcaseItem>2</ShowcaseItem>
                        <ShowcaseItem>3</ShowcaseItem>
                    </Row>   
                    <Row>
                        <ShowcaseItem>1</ShowcaseItem>
                        <ShowcaseItem>2</ShowcaseItem>
                        <ShowcaseItem>3</ShowcaseItem>
                    </Row>            
                </Container>       
        </Showcase>
    )
}

const Showcase = styled.div `
    display: block;
    height: 500px;
`
const ShowcaseItem = styled(Col)`

    background: red;
    height: 200px;
    margin: 10px;
    width: 25%;

`