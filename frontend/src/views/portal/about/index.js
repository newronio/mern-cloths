import React from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import styled from 'styled-components'
import TitlePage from '../../../components/titlePage'

//import { FcBearish } from "react-icons/fc";

export default () => {
    return (

        <About>
            <TitlePage title="About" sub="We Rise!"></TitlePage>
            <Description>
                <Container>
                    <h1> Texto</h1>
                </Container>
            </Description>

            <Collaborator>
                <Container>
                    <Row>
                        <BoxItem>1</BoxItem>
                        <BoxItem>2</BoxItem>
                        <BoxItem>3</BoxItem>
                        <BoxItem>4</BoxItem>
                    </Row>              
                </Container>            
            </Collaborator>
        </About>
    )
}

const About = styled.div`
    display: block;
    background: #ffbe76
`

const Description = styled.div`
    height: 400px;
    width: 100%;
    background: #ffbe76;
    display: flex;
`
const Collaborator = styled.div`
    height: 400px;
    background: grey;
    width: 100%;
    padding: 20px 0;
`
const BoxItem = styled(Col)`

    background: red;
    height: 200px;
    margin: 10px;
    width: 25%;

`