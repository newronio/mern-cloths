import React from 'react'
import Layout from '../../components/Layout'
import About from './about'
import Products from './products'
import Showcase from './showcase'
import Contact from './contact'
import Index from './index/index'
import { Route } from 'react-router-dom'



export default ({ match }) => {
    return (
            <Layout>
                <Route exact path={match.path + '/'} component={Index} />
                <Route exact path={match.path + 'about'} component={About} />
                <Route exact path={match.path + 'products'} component={Products} />
                <Route exact path={match.path + 'showcase'} component={Showcase} />
                <Route exact path={match.path + 'contact'} component={Contact} />
            </Layout>
            
    )
}


