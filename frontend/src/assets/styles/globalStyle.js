import { createGlobalStyle } from 'styled-components'
import 'bootstrap/dist/css/bootstrap.min.css';
import bgImg from '../img/bg.jpg'


const GlobalStyle = createGlobalStyle`
    * {
        margin: 0;
        padding: 0;
        outline: none;
        -webkit-font-smoothing: antialiased;
    }

     body {
        background: url(${bgImg}) no-repeat fixed;
        background-size:cover;
        text-rendering: optimizeLegibility; 
        align-items: center;
        height: 100vh;
        justify-content: center;
        text-rendering: optimizeLegibility;
    }
`

export default GlobalStyle