const express = require('express');
const Category = require('../../models/category');
const { check, validationResult } = require('express-validator');
const router = express.Router();

// @route    GET /category
// @desc     LIST category
// @access   Public
router.get('/', async (req, res, next) => {
    try {
      const category = await Category.find({})
      res.json(category)
    } catch (err) {
      console.error(err.message)
      res.status(500).send({ "error": "Server Error" })
    }
  })
  

// @route    POST /category
// @desc     CREATE category
// @access   Public
router.post('/', [
    check('name').not().isEmpty(),check('icon').not().isEmpty()
  ], async (req, res, next) => {
    try {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() })
      } else {
        let { name, icon } = req.body
        console.log(req.body)
        let category = new Category({ name, icon })
        await category.save()
        if (category.id) {
          res.json(category);
        }
      }
    } catch (err) {
      console.error(err.message)
      res.status(500).send({ "error": "Server Error" })
    }
  })


module.exports = router;